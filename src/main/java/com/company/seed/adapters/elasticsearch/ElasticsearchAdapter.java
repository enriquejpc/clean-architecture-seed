package com.company.seed.adapters.elasticsearch;

import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import com.company.seed.adapters.elasticsearch.model.UserElasticsearchModel;
import com.company.seed.application.port.out.UserElasticModelRepository;
import com.company.seed.application.port.out.UserRepository;
import com.company.seed.domain.User;

@Repository
@Slf4j
public class ElasticsearchAdapter implements UserRepository {

  UserElasticModelRepository userElasticModelRepository;

  public ElasticsearchAdapter(final UserElasticModelRepository userElasticModelRepository) {
    this.userElasticModelRepository = userElasticModelRepository;
  }

  @Override
  public User save(final User user) {
    log.info("Read from port.out {}", user);
    userElasticModelRepository.save(UserElasticsearchModel.DomainToElasticModel(user));
    log.info("Saved in ElasticSearch {}", user);
    return user;
  }
}
