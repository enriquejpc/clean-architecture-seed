package com.company.seed.adapters.elasticsearch.model;

import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.company.seed.domain.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "user.index")
public class UserElasticsearchModel {
  @Id
  String id;

  @Field(type = FieldType.Text)
  String mail;

  @Field(type = FieldType.Text)
  String password;

  public static User ElasticModelToDomain(UserElasticsearchModel user) {
    return User.builder()
      .mail(user.getMail())
      .password(user.getPassword())
      .build();
  }

  public static UserElasticsearchModel DomainToElasticModel(User user){
    return UserElasticsearchModel.builder()
      .mail(user.getMail())
      .password(user.getPassword())
      .build();
  }
}
