package com.company.seed.adapters.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.company.seed.domain.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KafkaProducerModel {

  Integer id;

  String mail;

  String password;

  public static User KafkaModelToDomain(KafkaProducerModel producerModel){
    return User.builder()
           .mail(producerModel.getMail())
           .password(producerModel.getPassword())
      .build();
  }

  public static KafkaProducerModel DomainToProducerModel(User user){
    return KafkaProducerModel.builder()
      .mail(user.getMail())
      .password(user.getPassword())
      .build();
  }
}
