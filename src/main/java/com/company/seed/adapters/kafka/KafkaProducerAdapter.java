package com.company.seed.adapters.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;
import com.company.seed.adapters.kafka.model.KafkaProducerModel;
import com.fasterxml.jackson.core.JsonProcessingException;

@Component
@Slf4j
public class KafkaProducerAdapter {

  @Autowired
  KafkaTemplate<String, KafkaProducerModel> kafkaTemplate;

  final String theTopic = "test-topic";

  public void sendMessage(KafkaProducerModel message) throws JsonProcessingException {
    kafkaTemplate.send(theTopic, message);
  }

}
