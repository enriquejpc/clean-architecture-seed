package com.company.seed.adapters.h2;

import org.springframework.stereotype.Repository;
import lombok.extern.slf4j.Slf4j;
import com.company.seed.adapters.h2.model.UserH2Model;
import com.company.seed.application.port.out.UserH2Repository;
import com.company.seed.application.port.out.UserRepository;
import com.company.seed.domain.User;

@Repository
@Slf4j
public class H2Adapter implements UserRepository {

UserH2Repository crudH2Repository;

  public H2Adapter(final UserH2Repository crudH2Repository) {
    this.crudH2Repository = crudH2Repository;
  }

  @Override
  public User save(final User user) {
    crudH2Repository.save(UserH2Model.DomainToUserH2Model(user));
    return user;
  }
}
