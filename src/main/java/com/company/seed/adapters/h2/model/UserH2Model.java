package com.company.seed.adapters.h2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import com.company.seed.domain.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserH2Model {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Integer id;

  String mail;

  String password;

  public static User H2ModelToDomain(UserH2Model userH2Model){
    return User.builder()
            .mail(userH2Model.getMail())
            .password(userH2Model.getPassword())
            .build();
  }

  public static UserH2Model DomainToUserH2Model(User user){
    return UserH2Model.builder()
      .mail(user.getMail())
      .password(user.getPassword())
      .build();
  }
}
