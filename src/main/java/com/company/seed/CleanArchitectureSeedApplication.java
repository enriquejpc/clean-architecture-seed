package com.company.seed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CleanArchitectureSeedApplication {

  public static void main(String[] args) {
    SpringApplication.run(CleanArchitectureSeedApplication.class, args);
  }

}
