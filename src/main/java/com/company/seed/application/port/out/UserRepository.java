package com.company.seed.application.port.out;

import com.company.seed.domain.User;

public interface UserRepository {
    User save(User user);
}
