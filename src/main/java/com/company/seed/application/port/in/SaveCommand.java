package com.company.seed.application.port.in;

import lombok.Builder;
import lombok.Value;
import com.company.seed.domain.User;

public interface SaveCommand {
  User save(Command user);

  @Value
  @Builder
  class Command {
    String id;

    String mail;

    String password;
  }
}
