package com.company.seed.application.port.out;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import com.company.seed.adapters.elasticsearch.model.UserElasticsearchModel;

public interface UserElasticModelRepository extends ElasticsearchRepository<UserElasticsearchModel, String> {
}
