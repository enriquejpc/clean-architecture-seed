package com.company.seed.application.port.out;

import org.springframework.data.repository.CrudRepository;
import com.company.seed.adapters.h2.model.UserH2Model;

public interface UserH2Repository extends CrudRepository<UserH2Model, Integer> {
}
